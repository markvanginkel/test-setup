
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="author" content="Hans Hendriks - Wepdefelopment - Foor al uw websites">

    <link rel="icon" type="image/png" href="/images/icon.png">

     <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <title>Nieuws.nl</title>
  </head>
  <body>
 

<div class="login-page">
  <div class="form">
    <h1>Nieuws.nl</h1>
    <h2>Inloggen</h2>
    <form class="login-form" method="post" action="inloggen.php">
      <input type="gebruikersnaam" placeholder="Gebruikersnaam" name="gebruikersnaam" />
      <input type="password" placeholder="wachtwoord" name="wachtwoord" />
      <button type="submit">login</button>
    </form>
    
    <p>
 		<a data-toggle="modal" data-target="#newAccount" class="btn btn-link">Nieuw account aanmaken</a>
 	</p>
  </div>
</div>


<!--
Onderstaande functie is nog in ontwikkeling.
Hans heeft hiervoor nog geen tijd gehad.....
!-->

<div id="newAccount" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nieuw account aanmaken</h4>
      </div>
      <div class="modal-body">
        <p>Vul onderstaande gegevens in om een nieuw account aan te maken</p>
		<!--<form class="login-form" name="newForm" method="post" action="makenewuser.php">!-->
      		
			<div class="form-group">
			  <label for="usr">Email:</label>
			  <input type="text" class="form-control" id="gebruikersnaam" name="gebruikersnaam" required="true" />
			</div>
			<div class="form-group">
			  <label for="pwd">Wachtwoord:</label>
			  <input type="password" class="form-control" id="wachtwoord" name="wachtwoord" required="true" />
			</div>
      		<button onclick="alert('Deze functie is nog in ontwikkeling....\n\nExcuses voor het ongemak....');" class="btn btn-success btn-lg">Account aanmaken</button>
   		 <!--</form>!-->
      </div>
      
      <div class="modal-footer">
      </div>

    </div>

  </div>
</div>


</body>
</html>